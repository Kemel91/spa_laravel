<?php

use Illuminate\Database\Seeder;
class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = file_get_contents(__DIR__ . '/sql/countries.sql');
        $regions = file_get_contents(__DIR__ . '/sql/regions.sql');
        $cities = file_get_contents(__DIR__ . '/sql/cities.sql');
        \DB::insert($countries);
        \DB::insert($regions);
        \DB::insert($cities);
    }
}
