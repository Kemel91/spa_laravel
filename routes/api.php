<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::namespace('Api')->group(function () {
    Route::get('/users', 'UsersController@index');
    Route::post('/users', 'UsersController@store');
    Route::get('/users/{user}', 'UsersController@show');
    Route::put('/users/{user}', 'UsersController@update');
    Route::delete('/users/{user}', 'UsersController@destroy');
    Route::get('/information/{key}','InformationController@show');
    Route::get('/cities','CityController@index');
    Route::get('/regions','RegionController@index');
    Route::get('/regions/{region}','RegionController@show');
    Route::get('/companies', 'CompanyController@index');
    Route::post('/companies','CompanyController@store');
    Route::get('/companies/{id}','CompanyController@show');
    Route::get('/reviews','ReviewController@index');
    Route::post('/reviews','ReviewController@store');
});
