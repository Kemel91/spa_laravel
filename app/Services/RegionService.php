<?php
namespace App\Services;

use App\Models\Region;

/**
 * Class InformationService
 * @package App\Services
 */
class RegionService
{
    /**
     * @param string $key
     * @return mixed
     */
    public function getAllRegions() {
        return Region::all();
    }

    /**
     * Достаем список городов по региону
     * и сортируем по имени
     * @param $id
     * @return mixed
     */
    public function getRegionCities($id) {
        return Region::where('id', $id) -> with(['cities' => function($query) {
            $query -> orderBy('name');
        }]) ->first();
    }

}
