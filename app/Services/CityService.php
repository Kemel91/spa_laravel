<?php
namespace App\Services;

use App\Models\City;

/**
 * Class InformationService
 * @package App\Services
 */
class CityService
{
    /**
     * @return mixed
     */
    public function getAllCities() {
        return City::all();
    }
}
