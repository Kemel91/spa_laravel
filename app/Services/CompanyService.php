<?php


namespace App\Services;

use App\Models\Company;

/**
 * Class CompanyService
 * @package App\Services
 */
class CompanyService
{
    /**
     * @return Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllCompanies() {
        return Company::where('id','>',0) -> with('city:id,name')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCompany($id) {
        return Company::where('id',$id)
                -> with(['city','reviews' => function($query) {
                    $query->orderBy('id','desc') -> take(10);
                }])
                -> firstOrFail();
    }
    /**
     * Сохраняем новую компанию
     * @param $data
     * @return bool
     */
    public function saveCompany($data) {
        $company = new Company($data);
        return $company -> save();
    }
}
