<?php


namespace App\Services;
use App\Models\Information;

/**
 * Class InformationService
 * @package App\Services
 */
class InformationService
{
    /**
     * @param string $key
     * @return mixed
     */
    public function getByKey(string $key) {
        return Information::where('key',$key) -> first();
    }
}
