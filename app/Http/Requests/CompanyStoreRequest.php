<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $name = request('name');
        $city_id = request('city_id');
        return [
            'city_id' => 'required|integer|exists:cities,id',
            'name' => ['required','string', Rule::unique('companies')
                ->where(function ($query) use ($name,$city_id) {
                return $query -> where('name',$name) -> where('city_id',$city_id);
            })],
            'address' => 'string|max:250|nullable',
            'email' => 'email|nullable',
            'site' => 'url|nullable',
            'description' => 'string|nullable',
            'file' => 'image',
        ];
    }
}
