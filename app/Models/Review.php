<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'company_id',
        'name',
        'text',
        'minus',
        'plus',
        'rating'
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function getNameAttribute($value) {
        return $value ?: 'Аноним';
    }
}
