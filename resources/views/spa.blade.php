<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:400,300,700,900" rel="stylesheet">
    <link href="{{ asset('font.min.css') }}" rel="stylesheet">
</head>
<header>
</header>
<body>

  <div id="app">
    <app></app>
  </div>
  <div id="copyrights">
      <div class="container">
        <p>
          &copy; Copyrights <strong>Kemel</strong>. Все права защищены
        </p>
        <div class="credits">
          Информация дополнительная
        </div>
      </div>
    </div>
  <script src="{{ mix('js/app.js') }}"></script>
</body>
</html> 