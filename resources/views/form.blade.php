@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" enctype="multipart/form-data" action="{{ secure_url('upload') }}">
            @csrf
            <input type="file" name="file">
            <button type="submit" value="Отправить">Отправить</button>
        </form>
    </div>
@endsection
